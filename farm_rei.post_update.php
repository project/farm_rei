<?php

/**
 * @file
 * Post update hooks for the farm_rei module.
 */

/**
 * Uninstall v1 migration configs.
 */
function farm_rei_post_update_uninstall_v1_migrations(&$sandbox) {
  $config = \Drupal::configFactory()->getEditable('migrate_plus.migration.farm_rei_input_log_field_rei');
  if (!empty($config)) {
    $config->delete();
  }
  $config = \Drupal::configFactory()->getEditable('migrate_plus.migration.farm_rei_material_type_term_field_rei');
  if (!empty($config)) {
    $config->delete();
  }
  $config = \Drupal::configFactory()->getEditable('migrate_plus.migration_group.farm_rei');
  if (!empty($config)) {
    $config->delete();
  }
}
